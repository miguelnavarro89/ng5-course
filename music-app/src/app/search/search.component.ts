import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  query: string;
  results: Object;

  constructor(
    private spotify: SpotifyService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route
      .queryParams
      .subscribe((params) => {
        this.query = params['query'] || '';
        console.log(`this.route.queryParams.subscribe()...`);
      });
  }

  ngOnInit() {
    this.search();
  }

  search() {
    console.log('this.query', this.query);
    if (!this.query) {
      return;
    }
    // Usar el servicio de Spotify
    this.spotify
      .searchTrack(this.query)
      .subscribe((res: any) => this.renderResults(res));
  }

  renderResults(res: any): void {
    console.log(res);
    this.results = null;
    if (res && res.tracks && res.tracks.items) {
      this.results = res.tracks.items;
    }
  }

  submit(queryValue: string) {
    // Cambia la ruta a '/search?query=....'
    this.router
      .navigate(['search'], { queryParams: { query: queryValue } })
      .then(() => {
        this.search();
      });
  }

}
