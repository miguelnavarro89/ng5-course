export interface Order {
  id: number;
  name: string;
  link: string;
  author: string;
};
