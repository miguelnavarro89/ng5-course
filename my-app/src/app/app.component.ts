import { Component } from '@angular/core';
import { Order } from './order.model';

@Component({
  selector: 'app-main',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'app works!';
  isLoading = false;
  myVar = 'asdasda';
  myOrders: Order[];

  constructor() {
    this.myOrders = [
      {id: 1, name: 'Order A', link: '#', author: 'Miguel'},
      {id: 2, name: 'Order B', link: '#', author: 'Oscar'},
      {id: 3, name: 'Order C', link: '#', author: 'Oscar'},
      {id: 4, name: 'Order D', link: '#', author: 'Oscar'},
      {id: 5, name: 'Order E', link: '#', author: 'Oscar'},
      {id: 6, name: 'Order F', link: '#', author: 'Oscar'},
    ];
  }

  onClick(event: any) {
    console.log('clicked: ', event);
  }

  onButtonLoaded(event: any) {
    console.log('loaded: ', event);
  }

}
