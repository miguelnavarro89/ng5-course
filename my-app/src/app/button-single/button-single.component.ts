import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

interface Order {
  id: number;
  name: string;
};

@Component({
  selector: 'button-single',
  templateUrl: './button-single.component.html',
  styleUrls: ['./button-single.component.css']
})
export class ButtonSingleComponent implements OnInit {
  @Input() color: string;
  @Input() size: string;
  @Input() style: string;
  @Input() state: string;
  @Input() isLoading = false;
  @Output() loaded: EventEmitter<boolean> = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

}
