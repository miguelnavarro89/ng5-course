import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'form-ng-model',
  templateUrl: './form-ng-model.component.html',
  styleUrls: ['./form-ng-model.component.css']
})
export class FormNgModelComponent implements OnInit {
  myForm: FormGroup;
  productName: string;

  constructor(fb: FormBuilder) {
    this.myForm = fb.group({
      'productName': [''],
    });
  }

  ngOnInit() {
    this.productName = 'Adidas';
  }

  onSubmit(formValue: string, event: any) {
    console.log(this.myForm);
    this.myForm.controls['productName'].setValue('Adidas changed!');
    setTimeout(() => {
      this.productName = 'Adidas Changed third time!';
    }, 1000);
  }

}
