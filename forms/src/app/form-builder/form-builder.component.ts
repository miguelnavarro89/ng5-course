import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validator } from '@angular/forms';
import { Validators } from '@angular/forms/src/validators';

const emailValidator = (control: FormControl): { [s: string]: boolean } => {
  if (!control.value.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)) {
    return { invalidEmail: true };
  }
}

@Component({
  selector: 'form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.css']
})
export class FormBuilderComponent implements OnInit {
  loginForm: FormGroup;
  formAlertMessage: string;
  formSubmitted = false;
  email;

  constructor(fb: FormBuilder) {
    this.loginForm = fb.group({
      'email': ['', Validators.compose([Validators.required, emailValidator])],
      'password': ['', Validators.required],
    });
  }

  ngOnInit() {}

  getFormAlertMessage(email: HTMLInputElement, password: HTMLInputElement): string {
    const controls = this.loginForm.controls;
    if (controls['email'].errors && controls['email'].errors['required']) {
      email.focus();
      return 'El campo email es requerido';
    } else if (controls['email'].errors && controls['email'].errors['invalidEmail'] === true) {
      email.focus();
      return 'El campo email es inválido';
    } else if (controls['password'].errors && controls['password'].errors['required'] === true) {
      password.focus();
      return 'El campo clave es requerido';
    }
  }

  onSubmit(email: HTMLInputElement, password: HTMLInputElement) {
    const formIsInvalid = this.loginForm.invalid;
    this.formSubmitted = true;
    if (formIsInvalid) {
      this.formAlertMessage = this.getFormAlertMessage(email, password);
    }
  }

}
