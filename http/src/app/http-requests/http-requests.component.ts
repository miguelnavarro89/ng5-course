import { Component, OnInit } from '@angular/core';
import {
  Http,
  Response,
  RequestOptions,
  Headers
} from '@angular/http';

@Component({
  selector: 'app-http-requests',
  templateUrl: './http-requests.component.html',
  styleUrls: ['./http-requests.component.css']
})
export class HttpRequestsComponent implements OnInit {
  data: Object;
  loading: boolean;

  constructor(private http: Http) { }

  ngOnInit() {
  }
  
  // SPA

  makePost() {
    this.loading = true;
    this.http.post(
      'http://jsonplaceholder.typicode.com/posts',
      JSON.stringify({
        title: 'Title 111',
        body: 'lorem ipsum',
        userId: 1,
      })
    ).subscribe((res: Response) => {
      this.data = res.json();
      this.loading = false;
    });
  }

  makeDelete() {
    this.loading = true;
    this.http
      .delete('http://jsonplaceholder.typicode.com/posts/1')
      .subscribe((res: Response) => {
      this.data = res.json();
      this.loading = false;
    });
  }

  makeHeaders() {
    const headers: Headers = new Headers();
    headers.append('X-API-TOKEN', 'ng-course1651456424684891841');

    const options: RequestOptions = new RequestOptions();
    options.headers = headers;

    this.loading = true;
    this.http
      .get('http://jsonplaceholder.typicode.com/posts/1', options)
      .subscribe((res: Response) => {
      this.data = res.json();
      this.loading = false;
    });
  }

}
