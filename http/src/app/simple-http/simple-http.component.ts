import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Post } from './post.model';

@Component({
  selector: 'app-simple-http',
  templateUrl: './simple-http.component.html',
  styleUrls: ['./simple-http.component.css']
})
export class SimpleHttpComponent implements OnInit {
  data: Post[];
  loading: boolean = false;

  // Dependency Injection
  constructor(private http: Http) {

  }

  ngOnInit() {
  }

  makeSingleRequest() {
    this.loading = true;
    const onSuccess = (response: Response) => {
      this.data = response.json();
      this.loading = false;
    };
    const onError = (error: Response) => {
      console.log('====================================');
      console.log(error.status);
      console.log('====================================');
      this.loading = false;
    };
    this.http
      .request('https://jsonplaceholder.typicode.com/posts/')
      .subscribe(onSuccess, onError);
  }

}
